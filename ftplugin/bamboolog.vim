if exists( 'b:did_ftplugin' )
   finish
endif
let b:did_ftplugin = 1

set foldexpr=getline(v:lnum)=~'Starting[[:space:]]task'?'>1':1
