" Vim configuration file

if $ETCDIR == ""
   if has("gui_win32")
      let $ETCDIR=expand("$HOME")
      let $ETCDIR.='\etc'
   elseif has("unix")
      let $ETCDIR=expand("$HOME")
      let $ETCDIR.='/etc'
   endif
endif

if $VARDIR == ""
   let $VARDIR = expand("$HOME")
   let $VARDIR .= '/var/vim'
endif

if ! isdirectory( expand("$VARDIR") )
   call mkdir( expand("$VARDIR"), 'p')
endif

" General options, viewing and changing settings {{{1
" Make the shell know it is running in vim.
let $VIMSESSION="vim" 

if isdirectory(expand("$ETCDIR") . "/vimrc")
   set runtimepath+=$ETCDIR/vimrc,$ETCDIR/vimrc/bundle/Vundle.vim
   set packpath+=$ETCDIR/vimrc
   if v:version >= 800
       set runtimepath+=$ETCDIR/vimrc/bundle/ale
   endif
endif

call vundle#begin("$ETCDIR/vimrc/bundle")
Plugin 'VundleVim/Vundle.vim'
Plugin 'kien/ctrlp.vim'
Plugin 'vhdirk/vim-cmake'
Plugin 'alepez/vim-gtest'
call vundle#end()

set number
set nocompatible
set nostartofline
set display=uhex
set showcmd
set laststatus=2
set ruler
set noerrorbells
set visualbell
set hidden
set backspace=indent,eol,start
set autowriteall
set autoread
set listchars=tab:>-,trail:*,eol:$
set scrolloff=1
set shiftwidth=4
set suffixes+=.d
set tabstop=4
set textwidth=0
set whichwrap=h,l
set timeoutlen=500
set ttimeoutlen=50
set cmdwinheight=12
set nojoinspaces

set splitright
set splitbelow

set path+=;,,

nmap ,l  :set invlist<CR>

if filereadable(expand("$ETCDIR") . "/vimrc/vimrc")
   nmap ,ve :edit $ETCDIR/vimrc/vimrc<CR>
   nmap ,vp :split $ETCDIR/vimrc/vimrc<CR>
   nmap ,vs :so $ETCDIR/vimrc/vimrc<CR>
endif
nmap \w   :write!<CR>

" Behave like the pager 'less'
nmap <Space> <C-F>

noremap q <nop>
noremap Q q
"because I've been known to accidentally hit "q" when I didn't
"mean it, only to find that I've been recording the last 5 minutes
"to some register.
"-tim

" Backups and viminfo file
set directory=~/tmp,/var/tmp,/tmp
set viminfo='20,<50,s10,h,n$VARDIR/viminfo
set nobackup
set nowritebackup

" Status line {{{1
set statusline=%<%f\ %h%m%r%y%{OptSet(&ic,'[ic]')}%{OptSet(&ws,'[ws]')}[%{Filencoding()}]\ %1*%{fnamemodify(getcwd(),':~')}%*%=%-14.(%l,%c%)\ %P/%L
function! OptSet(opt, string)
   if(exists("a:opt") && expand(a:opt))
      return a:string
   else
      return ""
   endif
endfunction

function! Filencoding()
   return eval("&fenc") . "*" . eval("&enc")
endfunction

function! SpellLang()
   if &spelllang == "fr"
      return "French"
   elseif &spelllang == "en"
      return "English"
   elseif $spelllang == "es"
      return "Spanish"
   endif
   return "Unknown"
endfunction

" Colors and syntax highlighting {{{1
syntax enable

set t_Co=256
set background=dark
set cursorcolumn
set cursorline
let g:solarized_termcolors=16
let g:solarized_bold      = 0
let g:solarized_underline = 0
let g:solarized_italic    = 0

colorscheme solarized

hi! link SpellBad   warningmsg
hi! link SpellCap   warningmsg
hi! link SpellRare  warningmsg
hi! link SpellLocal warningmsg

function! ToggleBackground()
    if w:solarized_style == "dark"
        let w:solarized_style = "light"
    else
        let w:solarized_style = "dark"
    endif
    colorscheme solarized
endfunction
nnoremap <F7> :call ToggleBackground()<CR>

function! ToggleHighlightListchars()
   if exists('w:highlight_listchars')
      call matchdelete(w:highlight_listchars)
      unlet w:highlight_listchars
      set nolist
   else
      let w:highlight_listchars = matchadd('preproc', '\t')
      set list
   endif
endfunction
nnoremap ,L :call ToggleHighlightListchars()<cr>

" Encoding {{{1
if has("multi_byte")
     set encoding=utf-8
     setglobal fileencoding=utf-8
     set termencoding=utf-8
     set fileencodings=utf-8,iso-8859-1
else
     echoerr "Sorry, this version of (g)vim was not compiled with +multi_byte"
endif
" Text editing, copying, pasting and folding  {{{1
nmap  Y y$
nmap ,o mzo<esc>`z
nmap ,O mzO<esc>`z
nmap ;o mzo<esc>kO<esc>`z
nmap ,i i <esc>r
nmap ,a a <esc>r
nmap ,<Space> a <esc>
nmap ;<Space> i <right> <esc>h
vmap ;<Space> <esc>`>a <esc>`<i <esc>
nnoremap J  gJ
nnoremap gJ J
nnoremap ]] ][
" nnoremap [[  ?{<CR>w99[{
set pastetoggle=<F12>
"Tip #1142: map . to .['
"nmap . .`[
nmap z{ [{V%zf<CR>
nmap z} [[V%zf<CR>
" Tip #282: Folding with Regular Expression
" See also foldutil.vim : Utility for creating folds (using specified match criteria)
set foldexpr=(getline(v:lnum)=~@/)?0:(getline(v:lnum-1)=~@/)\|\|(getline(v:lnum+1)=~@/)?1:2
map \z :set foldmethod=expr foldlevel=0 foldcolumn=2<CR>
autocmd FileType dosini FoldMatching \[\w\+\] -1

" Transferring information from one vim session to another
" Thanks to Charles E. campbell.
vmap \xw :w! /tmp/vimxfer<CR>
nmap \xr :r  /tmp/vimxfer<CR>

autocmd BufNewFile build.xml 0r ~/etc/vimrc/skeletons/build.xml
autocmd BufNewFile *.pl      0r ~/etc/idioms/perl/main.pl
autocmd BufNewFile *.rb      0r ~/etc/idioms/ruby/main.rb

" Format lists in text (:h fo-table :h flp)
" set flp=^\s*-\s*
set flp=^\\s*\\d\\+[\\]:.)}\\t\ ]\\s*

" Saving, backups, etc. {{{1
" A mapping to make a backup of the current file.
" Tip #853 from Anders Th�gersen
fun! WriteBackup()
        let fname = expand("%:p") . "__" . strftime("%Y_%m_%d_%H.%M.%S")
        silent exe ":w " . fname
        echo "Wrote " . fname
endfun
nnoremap <Leader>ba :call WriteBackup()<CR>

" Shorten one of the most often typed sequence:
imap <c-s> <esc>:w<CR>a

" Comments, indenting and other formatting options {{{1
set cinoptions+=(0
set expandtab
set comments+=nb:>
set formatoptions+=n

" File types and autocommands {{{1
filetype on
filetype plugin on
filetype indent on

augroup Java
  au BufReadPost  *.jar %!jar tv
  au BufReadPost  *.jar set ft=jar
augroup END

augroup Binary
  au!
  au BufReadPre   *.bin let &bin=1
  au BufReadPost  *.bin if &bin | %!xxd -u
  au BufReadPost  *.bin set ft=xxd | endif
  au BufWritePre  *.bin if &bin | %!xxd -r -u
  au BufWritePre  *.bin endif
  au BufWritePost *.bin if &bin | %!xxd -u
  au BufWritePost *.bin set nomod | endif
augroup END

augroup filetypedetect
  au! BufRead,BufNewFile *.otl		setfiletype vo_base
  au! BufRead,BufNewFile *.oln		setfiletype xoutliner
  au! BufRead,BufNewFile muttrc,.mutt/alias setfiletype muttrc
  au! BufRead,BufNewFile *.pas      setfiletype delphi
  au! BufRead,BufNewFile *.pp       setfiletype puppet
  au! BufRead,BufNewFile *.asm      setfiletype nasm
  au! BufRead,BufNewFile *.tex      setfiletype ft=tex
  au! BufRead,BufNewFile *.tex      set tw=80
  au! BufRead,BufNewFile *.ledger   setfiletype ledger | comp ledger
  au! BufNewFile,BufRead *.typ      set filetype=gtypist
  au! BufNewFile,BufRead plan-*.log set filetype=bamboolog
augroup END
  au! BufRead,BufNewFile *.xml      set fenc=utf-8

au! BufRead,BufNewFile *.xml set fenc=utf-8
"au! BufRead,BufNewFile *.xml set ro
"au! BufReadPost        *.xml %!xmltidy

au! StdinReadPost * set buftype=nofile

au! BufRead */vbox iab vbm VBoxManage

au! BufRead,BufNewFile *.proto set filetype=proto

au! BufEnter *.md setlocal spell spelllang=en_us

" Open the filetype plugin for the file's type
function! GetFileTypePlugin()
   let l:ftplugin = findfile( &ft . ".vim", $ETCDIR . "/vimrc/ftplugin")
   if ! strlen(l:ftplugin)
      echo "No filetype plugin found for type '" . &ft . "'"
      return
   endif
   execute "split " . l:ftplugin
endfunction
map ,vt :call GetFileTypePlugin()<CR>

" Insert shebang and modeling according
" to file's type.
function! Shebang()
   if exists("b:shebang") 
      if match(getline(1), b:shebang) == -1
         call append(0, b:shebang)
      endif
   else
      echo "No shebang line defined for file type '". &filetype . "'"
   endif
   if exists("b:modeline") 
      if match(getline(line('$')), b:modeline) == -1
         call append(line('$'), b:modeline)
      endif
   else
      echo "No modeline defined for file type '". &filetype . "'"
   endif
endfunction
map <F3> :call Shebang()<CR>

function! PrettyPrint()
   normal ma
   " %!astyle --options=$HOME/.astylerc
   normal `a
endfunction

au! BufWritePre *.cc call PrettyPrint()
au! BufWritePre *.h  call PrettyPrint()

" Buffers and windows {{{1
nmap ,cd  :cd %:p:h<CR>
nmap + <C-W>+
nmap - <C-W>-
nmap <c-h> <C-W><
nmap <c-l> <C-W>>
nmap ;h :A<CR>
nmap ;H :AS<CR>
"nmap <F3> :call ScratchBuffer()<CR>
function! ScratchBuffer()
   new
   set buftype=nofile
   set bufhidden=hide
   setlocal noswapfile
endfunction

" Completion {{{1
set suffixes+=.aux,.log
set wildignore+=*.o,*.pdf
set wildmode=list:longest
set wildmenu
set completeopt-=preview

" Diff {{{1
set diffopt=filler,iwhite

" Printing Options {{{1
set printoptions=syntax:n,paper:letter

" Command line configuration {{{1
set history=50
set cmdheight=2
cnoremap  <C-A>      <Home>
cnoremap  <C-B>      <Left>
"cnoremap <C-D>      <Del> conflicts with completion
cnoremap  <C-E>      <End>
"cnoremap <C-F>      <Right> conflicts with Ex window
cnoremap  <C-N>      <Down>
cnoremap  <C-P>      <Up>
cnoremap  <Esc><C-B> <S-Left>
cnoremap  <Esc><C-F> <S-Right>


" Searching, tagging and indexing  {{{1
runtime! crypto.vim
runtime! search.vim
set incsearch
set hlsearch
nnoremap <C-L> :nohls<CR><C-L>
set ignorecase
set smartcase
nmap n nzz
nmap N Nzz
set tags=tags;
nmap <c-]> :exe 'tjump ' . expand("<cword>")<cr>
nmap ;j :exe 'g/' . expand("<cword>") . '/#'<cr>
set makeef=errors.err

command! MakeTags !ctags -R .

set cscopequickfix=s-,c-,d-,i-,t-,e-

nnoremap <silent> ;gg :silent call Glimpse( expand('<cword>') )<cr>
vnoremap <silent> ;gg :<c-u>silent call VisualGlimpse()<cr>
nnoremap <silent> ;gl :silent call MatchingFileGlimpse( expand('<cword>') )<cr>
vnoremap <silent> ;gl :<c-u>silent call VisualMatchingFileGlimpse()<cr>
nnoremap <silent> ;gi :silent call GlimpseIndex()<cr>
nnoremap <silent> ;gr :silent call GitGrep( expand('<cword>') )<cr>
vnoremap <silent> ;gr :<c-u>silent call VisualGitGrep()<cr>

" Go to next quickfix error
nmap <C-N> :cnext<cr>
nmap <C-P> :cprevious<cr>

" grep online dictionary
map ;gd :!webster <cword><CR>
"map ;gr :new +r!elinks\ -dump\ -no-numbering\ -no-references\ "http://www.webster.com/cgi-bin/dictionary?sourceid=Mozilla-search&va=<cword>"

" Use man pages from within vim
runtime! ftplugin/man.vim
nmap K :Man <C-R>=expand("<cword>")<CR><CR>

" GNU Global {{{1
nnoremap <silent> \gd :Gtags <c-r>=expand('<cword>')<cr><cr>
nnoremap <silent> \gr :Gtags -r <c-r>=expand('<cword>')<cr><cr>
nnoremap <silent> \gg :Gtags -g <c-r>=expand('<cword>')<cr><cr>
nnoremap <silent> \gc :GtagsCursor<cr>

" Compilers, syntax checkers and other filters {{{1
nmap ;m :make!<CR>:copen<CR>
nmap ;t :make! runtest<CR>:cwindow<CR>

highlight! link GcovUncovered DiffText

" Tip #1141: command PP: print lines like :p or :# but with with current search pattern highlighted {{{1
" command PP: print lines like :p or :# but with with current search pattern highlighted
command! -nargs=? -range -bar PP :call PrintWithSearchHighlighted(<line1>,<line2>,<q-args>)
function! PrintWithSearchHighlighted(line1,line2,arg)
    let line=a:line1
    while line <= a:line2
        echo ""
        if a:arg =~ "#"
            echohl LineNr
            echo strpart("       ",0,7-strlen(line)).line."\t"
            echohl None
        endif
        let l=getline(line)
        let index=0
        while 1
            let b=match(l,@/,index)
            if b==-1 |
                echon strpart(l,index)
                break
            endif
            let e=matchend(l,@/,index) |

            echon strpart(l,index,b-index)
            echohl Search
            echon strpart(l,b,e-b)
            echohl None
            let index = e
        endw
        let line=line+1
    endw
endfu 

" Settings for Taglist plugin {{{1
let Tlist_Ctags_Cmd = "/usr/bin/ctags"
let Tlist_Show_One_File = 0
let Tlist_Use_Right_Window = 1
let Tlist_Exit_OnlyWindow = 1
let Tlist_File_Fold_Auto_Close = 1
let Tlist_Compact_Format = 0
let Tlist_Display_Prototypes = 1
let Tlist_WinWidth = 50
let Tlist_Sort_Type = "name"
" I to add the following when the rxvt
" window started closing on me on a resize.
" This was in cygwin.
let Tlist_Inc_Winwidth = 0
let tlist_delphi_settings='delphi;f:function'
let tlist_promela_settings='promela;p:package;m:mtype;c:channel'
let tlist_proto_settings='proto;m:message;e:enum'

nmap ,t :Tlist<CR>



let g:Tex_UsePython=0

" Spell checkers {{{1
nmap ,se :set spelllang=en<cr>:set spellfile=~/etc/vimrc/spell/en-utf-8.add<cr>
nmap ,sf :set spelllang=fr<cr>:set spellfile=~/etc/vimrc/spell/fr-utf-8.add<cr>
nmap ,ss :set spelllang=es<cr>:set spellfile=~/etc/vimrc/spell/es-utf-8.add<cr>

" Creating content {{{1
" Insert date/time
" YYYY-MM-DD format (ISO)
nmap ,d i<c-r>=strftime("%Y-%m-%d")<cr><esc>
" print a ruler
nmap [r O....+....1....+....2....+....3....+....4....+....5....+....6....+....7....+....80<esc>0
" insert current file's name
nmap ,f i<c-r>=expand('%:t:r')<cr><esc>

" Settings for ALE {{{1
let g:ale_python_flake8_args="--max-line-length=120"
let g:ale_python_pycodestyle_options="--max-line-length=120"

" Settings for NERDTree plugin {{{1
nmap \nt :NERDTreeToggle<cr>

" Settings for LookupFile plugin {{{1
let g:LookupFile_TagExpr='findfile("filenametags", ";")'
let g:LookupFile_MinPatLength=3
let g:LookupFile_PreserveLastPattern=0
nmap \lf <Plug>LookupFile
nmap \lw <Plug>LUWalk

nmap \lg :GlimpseLookup<CR>


" Settings for Project plugin {{{1
nmap <silent> ,p <Plug>ToggleProject
let g:proj_window_width=25
let g:proj_window_increment=30
let g:proj_flags="iLsStc"
let g:proj_run1="!ctags -f %d/.tags -R %d"
let g:proj_run2="!lookupfile-tag.sh"
let g:proj_run3="!printf \".tags\\n.filenametags\\ndoc/\*\\n\" > .glimpse_exclude && glimpseindex -H %d -o %d"

" Settings for ant {{{1
let g:tlist_ant_settings = 'ant;p:Project;t:Target;r:Property'
let g:tlist_ant_settings = 'ant;p:Project;t:Target'

" Settings for CDL files (eCos configurations) {{{1
let g:tlist_cdl_settings = 'cdl;p:Package;c:Component;o:Option'

" Settings for netrw {{{1
let g:netrw_scp_cmd = "scp -q"
let g:netrw_home = $VARDIR

" Settings for EnhCommentify {{{1
let g:EnhCommentifyRespectIndent='yes'
au BufEnter *.ledger let g:EnhCommentifyRespectIndent='no'
let g:EnhCommentifyPretty='yes'
let g:EnhCommentifyFirstLineMode='yes'
" function! EnhCommentifyCallback(ft)
   " if a:ft == 'ledger'
      " let b:ECcommentOpen = ';'
      " let b:ECcommentClose = ''
   " endif
" endfunction
" let g:EnhCommentifyCallbackExists = 'yes'

" Settings for snippets plugin {{{1
let g:snippets_base_directory = expand("$ETCDIR/vimrc/snippets")

" Settings for yankring plugin {{{1
let g:yankring_history_dir='~/etc/vimrc'
nnoremap <silent> ,yy :YRShow<CR>
" Settings for imaps plugin {{{1
"nmap ,f <plug>IMAP_JumpForward

" Settings for ant build files {{{1
let g:tlist_ant_settings = 'ant;p:Project;t:Target'

" Settings for dbext plugin {{{1
let g:dbext_default_SQLITE_bin = 'sqlite3'
" Ex commands
" :DBExecSQL Enter arbitrary SQL
"
" Visual mode mappings
" \se  " Execute visually selected SQL
" \st  " select * from visually selected table
" \sdt " describe visually selected table
" \slc " display a list of columns for visually selected table
"
" Normal mode mappings
" \se  " execute SQL under cursor
" \st  " select * from visually selected table
" \sta " ask for a table name and select * from it
" \sd  " describe table under cursor
" \sh  " show history of previous SQL statements

" Settings for manpageview plugin {{{1
let g:manpageview_winopen = 'hsplit='

" Settings for bufexplorer {{{1
let g:bufExplorerSplitBelow=1
let g:bufExplorerFindActive=0

" Settings for latex-suite {{{1
let g:Tex_ViewRule_pdf = 'evince'
let g:Tex_DefaultTargetFormat = 'pdf'
let g:Tex_GotoError = 0

" Settings for ledger {{{1
let g:ledger_detailed_first = 1
" nmap <CR> :!ledger --sort d --format '\%d \%X \%-P \%o\n' --current reg <cWORD><cr>
" nmap <CR> :!ledger --sort d -R --current reg <cWORD><cr>
nmap \la  :!ledger -B -s -E --current           bal '^Assets'<cr>
" nmap \lc  :!ledger -B -s -E --current --cleared bal '^Assets'<cr>
nmap \lr  :!ledger -B -s -R --current --cleared bal '^Assets'<cr>
" Use to sum transaction items
function! Sum() range
   let l:total = 0
   let l:lineno = a:firstline
   while( l:lineno <= a:lastline )
      let l:line = getline(l:lineno)
      let l:item = str2float( matchstr(l:line, '-\?\d\+\.\d\+') )
      let l:total += l:item
      let l:lineno += 1
   endwhile
   echo l:total
endfunction
vmap \lt  :call Sum()<cr>
nmap \lc  :call LedgerToggleTransactionState(line('.'))<cr>

" Pattern repository (should be moved to another file) {{{1
" Adding commas to numbers: s/\v(\d)\zs\ze((\d\d\d)+)\D/,/
" Same but adding spaces to hex numbers: s/\v\x\zs\ze((\x\x\x\x)+)$/ /
"
" s/^.*\(..\)\.sdata/\=printf("tvr_%d=%s", "0x" .submatch(1), submatch(0))
"
" From Tim Chase in the vim list:
" :g/^Chapter/t.|s/./=/g
" which will find all the lines that begin with "Chapter" and put 
" an equal number of "=" signs below
"
" Also:
" map \= yypVr=

function! Sdata()
   let l:line = getline(".")
   let l:line = substitute(l:line, "^.*\\(..\\)\.sdata", '\=printf("tvr_%d=%s", "0x".submatch(1), submatch(0))', "")
   call setline(".", l:line)
endfunction

" Execute current line in shell and append output
" after current line.
function! Shell()
   let l:line=getline(".")
   " Remove any prompt characters
   let l:line=substitute(l:line, "^\\H*\\(.*\\)", '\=submatch(1)', "")
   let l:line=escape(l:line, '!#%')
   exec "read!" . l:line
endfunction
map \lx :call Shell()<CR>

" Execute command in shell and send
" result to a new window
function! ShellCommand()
    let shell_command = input("$ ")
    echo "Command to execute: " . shell_command
endfunction
map \ls :call ShellCommand()<CR>

function! DoEx()
   let l:saved = @"
   let l:line=getline(".")
   redir @"
   exec l:line
   redir END
   put "
   let @"=l:saved
endfunction
map \lv :call DoEx()<CR>

" See :h abbreviations
function! Eatchar(pat)
   let c = nr2char(getchar(0))
   return (c =~ a:pat) ? '' : c
endfunc

if filereadable("$ETCDIR/vimrc/gvimrc")
   so $ETCDIR/vimrc/gvimrc
endif

" Pretty-print register content
let s:number = '0123456789'
let s:letter = 'abcdefghijklmnopqrstuvw'
let s:special = '.-+/*:#'
let s:all = s:number.s:letter.s:special

let s:kinds = { 'number' : s:number , 'letter' : s:letter , 'special' : s:special , 'all' : s:all }

func! s:Register( kind_or_literal )
  if empty(a:kind_or_literal)
    let register = s:all
  elseif has_key(s:kinds,a:kind_or_literal)
    let register = s:kinds[a:kind_or_literal]
  else
    let register = a:kind_or_literal
  endif

  echohl Title
  echo 
"---Register(".(empty(a:kind_or_literal)?"all":a:kind_or_literal).")---"
  echohl None
  for r in split(register,'\ze')
    try
      let content = eval('@'.r)
    catch
      echohl Error
      echo 'No such register : '.r
      echohl None
    endtry
    if !empty(content) || strlen(a:kind_or_literal) == 1
      echohl Special
      echo '"'.r
      echohl None
      echo content
    endif
  endfor
endfun

" File opening functions {{{1
" See also log4j.vim in ftplugin
" function! OpenJavaFile(classname)
"    let l:filename = substitute(a:classname, '\.', '/', 'g')
"    let l:filename = fnamemodify(l:filename, ':h')
"    exe "split Java_src/" . l:filename . ".java"
" endfunction
" 
" nnoremap <silent> <F6> :call OpenJavaFile( expand('<cfile>') )<CR>
function! OpenFileGotoLine(split)
   setlocal isfname+=:
   let l:destination = split( expand('<cfile>'), ':')
   setlocal isfname-=:
   let l:filename = findfile( l:destination[0] )
   if l:filename == ''
      echo "File not found in 'path': " . &path
      return
   endif
   if a:split == 1
     exe 'split +' . l:destination[1] . ' ' . expand(l:filename)
  else
     exe 'edit +' . l:destination[1] . ' ' . expand(l:filename)
   endif
endfunction
nnoremap <silent> gl :call OpenFileGotoLine(1)<CR>
nnoremap <silent> gL :call OpenFileGotoLine(0)<CR>

func! RegisterComplete(a,b,c)
  return substitute(s:all,'\ze',"\n",'g')."\nall\nnumber\nletter"
endfun

com! -nargs=* -complete=custom,RegisterComplete -bar Register call s:Register(<q-args>)

" Source host-specific settings {{{1
exe "runtime! " . hostname() . ".vim"

" Source directory specific settings {{{1
if filereadable("./settings.vim")
   source ./settings.vim
endif

" vim: set fdm=marker tw=0:
