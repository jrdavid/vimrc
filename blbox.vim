function! LaunchTest()
    let l:this_file = expand('%:t:r')
    let l:i = matchend(l:this_file, '^check-')
    if l:i >= 0
        let l:this_file = strpart(l:this_file, l:i)
    endif
    exe "make test-" . l:this_file
endfun

map \t :call LaunchTest()<cr>

set wildignore +=*.t
