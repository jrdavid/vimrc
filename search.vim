" search: various useful tools for searching text.
"  Author: Jean-Rene David
"  Functions:
"     VisualSearch: search for visually selected text
"     Glimpse: search for word under cursor with glimpse
"     VisualGlimpse: search for visually selected text with glimpse
"     GlimpseIndex: index current directory recursively
"     SearchLine: search for lines identical to the current one
"  Mappings:
"     See each section for the mappings associated with each
"     function.

nnoremap <silent> <F10> :set invhls<CR>

" Highlight current word {{{1
" Just highlight the current word witout moving
nmap & :let @/=expand("<cword>")<CR>:set hls<CR>

" Search for visually selected text {{{1
" From an idea by Michael Naumann, J�rgen Kr�mer.
function! VisualSearch(direction) range
   let l:saved_reg = @"
   execute "normal! vgvy"
   let l:pattern = escape(@", '\\/.*$^~[]')
   let @/ = l:pattern
   let @" = l:saved_reg
   if a:direction == 'f'
      normal n
   else
      normal N
   endif
endfunction

function! SearchInOtherFile()
   let l:other_file="/tmp/string_file_list"
   let l:saved_reg = @"
   execute "normal! vgvy"
   let l:pattern = escape(@", '\\/.*$^~[]')
   let @/ = l:pattern
   let @" = l:saved_reg
   exe "split " . l:other_file
   normal n
endfunction

vnoremap <silent> \q :call SearchInOtherFile()<cr>

vnoremap <silent> * :call VisualSearch('f')<CR>
vnoremap <silent> # :call VisualSearch('b')<CR>

" Glimpse {{{1
" Since vim now has its own internal grep,
" we use the old :grep command to use glimpse
" instead.
set grepformat+=%f:\ %l:%m
set grepformat+=%f

" This function is a wrapper around :grep.
" It is just used to set 'shellpipe' to '>' temporarily.
" There is no need to see the results on the screen and
" it slows things down.
function! Glimpse(pattern)
   let old_grepprg=&grepprg
   let old_shellpipe=&shellpipe
   " Compose options to pass to glimpse.
   " This is all just to honor &ic
   let l:glimpse_options = "-ny"
   if &ignorecase
      let l:glimpse_options = l:glimpse_options . "i"
   endif
   " Search for index in current directory and parents
   let l:directory = findfile('.glimpse_index', ';')
   if l:directory == ""
      echo "No index file found in '" . getcwd() . "' or parents."
      return
   else
      let l:directory = fnamemodify(l:directory, ':p:h')
   endif
   let l:glimpse_options = l:glimpse_options . "H\\ " . l:directory
   exe "set grepprg=glimpse\\ " . l:glimpse_options . "\\ $*"
   set shellpipe=>
   let l:maxlen = 27 " Maximum pattern length accepted by glimpse
   if strlen(a:pattern) > l:maxlen 
     let l:pattern = strpart(a:pattern, 0, l:maxlen)
     echo "Truncating pattern to " . l:maxlen . " characters: " . l:pattern
   else
     let l:pattern = a:pattern
   endif
   exe 'grep! ' . l:pattern
   let &shellpipe=old_shellpipe
   let &grepprg=old_grepprg
   botright cwindow
endfun

function! GlimpseIndex()
   let l:cwd = getcwd()
   exe "!glimpseindex -M 32 -H " . l:cwd . " -o " . l:cwd
endfun

function! VisualGlimpse() range
   let l:saved_reg = @"
   execute "normal! vgvy"
   let l:pat = escape(@", '\\/.*$^~[]')
   let l:pat = substitute(l:pat, "\n$", "", "")
   call Glimpse('"' . l:pat . '"')
   let @/=strpart(l:pat, 0, 27)
   let @" = l:saved_reg
endfunction

function! MatchingFileGlimpse(pattern)
   call Glimpse('-l "' . a:pattern . '"')
endfun

function! VisualMatchingFileGlimpse() range
   let l:saved_reg = @"
   execute "normal! vgvy"
   let l:pat = escape(@", '\\/.*$^~[]')
   let l:pat = substitute(l:pat, "\n$", "", "")
   call Glimpse('-l "' . l:pat . '"')
   let @/=strpart(l:pat, 0, 27)
   let @" = l:saved_reg
endfunction

command! -nargs=1 Glimpse call Glimpse(<f-args>)

"GitGrep {{{1
function! GitGrep(pattern)
   let old_grepprg=&grepprg
   let old_shellpipe=&shellpipe
   " Compose options to pass to gitgrep.
   " This is all just to honor &ic
   let l:gitgrep_options = "-n"
   if &ignorecase
      let l:gitgrep_options = l:gitgrep_options . "i"
   endif
   exe "set grepprg=git\\ grep\\ " . l:gitgrep_options . "\\ $*"
   set shellpipe=2>\ /dev/null\ >
   exe 'silent grep! ' . a:pattern  
   " exe 'silent grep! "' . a:pattern . '"'
   let &shellpipe=old_shellpipe
   let &grepprg=old_grepprg
   botright cwindow
endfun!

function! VisualGitGrep() range
   let l:saved_reg = @"
   execute "normal! vgvy"
   let l:pattern = escape(@", '\\/.*$^~[]')
   let l:pattern = substitute(l:pattern, "\n$", "", "")
   call GitGrep(l:pattern)
   let @/ = l:pattern
   let @" = l:saved_reg
endfunction

command! -nargs=1 GitGrep call GitGrep(<f-args>)

" SearchLine {{{1
" Search for lines identical to the current one.
" Not currently mapped to anything. But it might
" go like this:
"
"noremap <silent> & :call SearchLine('f')<CR>
function! SearchLine(direction) range
   let l:saved_reg = @"
   yank
   let l:pattern = escape(@", '\\/.*$^~[]')
   let l:pattern = substitute(l:pattern, '\n', '', '')
   let @/ = l:pattern
   let @" = l:saved_reg
   if a:direction == 'f'
      normal n
   else
      normal N
   endif
endfunction

" Settings for Grep.vim plugin {{{1
nmap ;gb :exe "Bgrep " . expand("<cword>")<CR>

" Extensions to lookupfile plugin {{{1
function! GlimpseFileNames(pattern)
   let matches = []
   let g:LookupGlimpsePat = escape(a:pattern, '\\/.*$^~[]')
   let files = system("glimpse -liH " . getcwd() . " " . g:LookupGlimpsePat)
   let matches = split(files, "\n")
   return matches
endfunction

function! MyNotify()
  unlet g:LookupFile_LookupFunc g:LookupFile_LookupNotifyFunc
  let g:LookupFile_LookupFunc = g:savedLookupFunc
  let g:LookupFile_LookupNotifyFunc = g:savedLookupNotifyFunc
  let g:LookupFile_MinPatLength =  g:savedLookupMinPatLength
  let @/ = g:LookupGlimpsePat
endfunction

function! GlimpseLookup()
        unlet! s:savedLookupFunc s:savedLookupNotifyFunc
        let g:LookupGlimpsePat=""
        let g:savedLookupFunc = g:LookupFile_LookupFunc
        let g:savedLookupNotifyFunc = g:LookupFile_LookupNotifyFunc
        let g:savedLookupMinPatLength = g:LookupFile_MinPatLength
        let g:LookupFile_MinPatLength=5
        unlet g:LookupFile_LookupFunc g:LookupFile_LookupNotifyFunc
        let g:LookupFile_LookupFunc = 'GlimpseFileNames'
        let g:LookupFile_LookupNotifyFunc = 'MyNotify'
        LookupFile
endfunction

command! GlimpseLookup call GlimpseLookup()

" vim: fdm=marker :
