PROG=vim
cwd = $(shell pwd)

install:
	mkdir -p $(HOME)/var/$(PROG)
	ln -sf $(cwd)/vimrc    $(HOME)/.vimrc
